# Renovate test harness

An example test harness for writing tests against Renovate configuration, with multiple types of functionality:

## For Custom Managers (with regexes)

An example test harness for writing tests against Renovate's [Custom Manager Support using Regex](https://docs.renovatebot.com/modules/manager/regex/).

Corresponds with the blog post [_Creating a test harness for validating Renovate regex manager rules_](https://www.jvt.me/posts/2024/06/28/renovate-regex-test/).

## For Custom Datasources

An example test harness for writing tests against Renovate's [Custom Datasources](https://docs.renovatebot.com/modules/datasource/custom/).

Corresponds with the blog post [_Creating a test harness for validating Renovate Custom Datasource configuration_](https://www.jvt.me/posts/2025/03/01/renovate-custom-datasource-test/).

## License

Licensed under the Apache-2.0 License.
