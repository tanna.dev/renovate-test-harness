import { extractPackageFile } from 'renovate/dist/modules/manager/custom/regex'
import { ExtractConfig } from 'renovate/dist/modules/manager/types'
import jsonata from 'jsonata'
import { readFileSync } from 'fs'

describe('renovate.json', () => {
	const baseConfig = require('./renovate.json')
	const packageFile = 'UNUSED'

	describe('buildkite images', () => {
		// unfortunately we have to index into this right now, until https://github.com/renovatebot/renovate/issues/21760 is complete
		const config: ExtractConfig = baseConfig.customManagers[0]

		const fileContents = {
			'image with tag and quotes': `
			image: "golang:1.19"
			`,
			'image with tag but no quotes': `
			image: golang:1.19
			`,
			'image with tag and digest and quotes': `
			image: "golang:1.22@sha256:0b55ab82ac2a54a6f8f85ec8b943b9e470c39e32c109b766bbc1b801f3fa8d3b"
			`,
		}

		it('matches an image with tag and quotes', () => {
			const content = fileContents['image with tag and quotes']

			const res = extractPackageFile(content, packageFile, config)

			expect(res).toMatchSnapshot({
				deps: [
					{
						depName: 'golang',
						currentValue: '1.19'
					}
				]
			})
		})

		it('matches an image with tag but no quotes', () => {
			const content = fileContents['image with tag but no quotes']

			const res = extractPackageFile(content, packageFile, config)

			expect(res).toMatchSnapshot({
				deps: [
					{
						depName: 'golang',
						currentValue: '1.19'
					}
				]
			})
		})

		it('matches an image with tag and digest and quotes', () => {
			const content = fileContents['image with tag and digest and quotes']

			const res = extractPackageFile(content, packageFile, config)

			expect(res).toMatchSnapshot({
				deps: [
					{
						depName: 'golang',
						currentValue: '1.22',
						currentDigest: 'sha256:0b55ab82ac2a54a6f8f85ec8b943b9e470c39e32c109b766bbc1b801f3fa8d3b'
					}
				]
			})
		})
	})

	describe('custom datasources', () => {
		// NOTE that we should test to validate there are Custom Datasources defined

		describe('hashicorp consul', () => {
			// NOTE that we should test to validate this is defined before referencing it
			const datasource = baseConfig.customDatasources!['hashicorp-consul']

			it('transforms correctly', () => {
				const contents = readFileSync('./__testdata/hashicorp-consul-oss.json')
				const apiResponse = JSON.parse(contents.toString('utf8'))

				// NOTE that we may need to validate there's only 1 entry in `transformTemplates`
				const template = datasource.transformTemplates![0]
				const expression = jsonata(template)

				const expected = {
					"releases": [
						{
							"version": "1.20.1",
							"releaseTimestamp": "2024-10-30T16:59:30.860Z",
							"changelogUrl": "https://github.com/hashicorp/consul/blob/release/1.20.1/CHANGELOG.md",
							"sourceUrl": "https://github.com/hashicorp/consul"
						},
						{
							"version": "1.18.2",
							"releaseTimestamp": "2024-05-17T14:41:12.287Z",
							"changelogUrl": "https://github.com/hashicorp/consul/blob/release/1.18.2/CHANGELOG.md",
							"sourceUrl": "https://github.com/hashicorp/consul"
						},
					],
					"homepage": "https://www.consul.io"
				}

				// as this returns a promise, and we need to resolve it, but then modify the actual response
				expression.evaluate(apiResponse).then(function(rawActual: any) {
					// For some reason, we're being hit with https://github.com/jsonata-js/jsonata/issues/296
					const actual = JSON.parse(JSON.stringify(rawActual))

					expect(actual).toStrictEqual(expected)
				})
			})
		})
	})
});
